import db
import PySimpleGUI as sg
#import aspnewtest
import plc_com
import apis
#import sapnew
import sap_com
import schedule
import gui_backend
import threading
import datetime
import time


########################Shift decide Block##################################################
shift_a_start=datetime.time(5,0,0,0)
shift_b_start=datetime.time(18,30,0,0)
shift_a_end=datetime.time(18,30,0,0)
shift_b_end=datetime.time(5,0,0,0)
def get_curr_shift():
    global shift_a_start,shift_a_end,shift_b_end,shift_b_start
    now=datetime.datetime.now().time()
    new_day=datetime.time(23,59,0,0)
    new_one=datetime.time(0,0,0,0)
    if shift_b_start<now<new_day or new_one<now<shift_b_end:
        return('B')
    elif shift_a_start<now<shift_a_end:
        return('A')
    else:
        return(False)
#############################################################################################
def take_sample():
    global order_s,prod_s,reject_s,rework_s
    print('running hourly')
    #data=["2,shaping,12,Base1,Activity 2,3,21:09:2018,00:21,A,10,11,6:43,YES, , , , , , , , , , , , , ,Work done","2,shaping,12,Base1,Activity 2,3,21:09:2018,00:21,A,10,11,6:43,YES, , , , , , , , , , , , , ,Work done","2,shaping,12,Base1,Activity 2,3,21:09:2018,00:21,A,10,11,6:43,YES, , , , , , , , , , , , , ,Work done"]
    #sap_com.sap_send(order_s,prod_s,reject_s,rework_s)

def end_shift_only():
    global order_s,reject_s,rework_s,status
    status = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(10):
        all_val = plc_com.get_plc_data(i)
        value_1 = all_val[0]
        db.put_in_sap(str(i),str(value_1[4]),shift,value_1[6],1)
        print(apis.send_stat([mach_names[i], '0']))
        plc_com.change_pc(i, 1)
        plc_com.change_ack_value(i, 1)
    sap_com.sap_send()
    db.drop_sap_send()
    sap_com.recv_sap()
    db.log_update()
    # sap_com.recv_sap()
    #time.sleep(10)
    #int('a')
    #data=["2,shaping,12,Base1,Activity 2,3,21:09:2018,00:21,A,10,11,6:43,YES, , , , , , , , , , , , , ,Work done","2,shaping,12,Base1,Activity 2,3,21:09:2018,00:21,A,10,11,6:43,YES, , , , , , , , , , , , , ,Work done","2,shaping,12,Base1,Activity 2,3,21:09:2018,00:21,A,10,11,6:43,YES, , , , , , , , , , , , , ,Work done"]

    #sap_com.sap_send(order_s,prod_s,reject_s,rework_s)

def end_shift_only_b():
    global order_s,reject_s,rework_s,status
    status = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(10):
        all_val = plc_com.get_plc_data(i)
        value_1 = all_val[0]
        db.put_in_sap(str(i),str(value_1[4]),shift,value_1[6],0)
        try:
            print(apis.send_stat([mach_names[i], '0']))
        except Exception as e:
            print(e)
        plc_com.change_pc(i, 1)
        plc_com.change_ack_value(i, 1)
    sap_com.sap_send()
    db.drop_sap_send()
    sap_com.recv_sap()
    db.log_update()


def run_hourly():
    t1=threading.Thread(target=take_sample)
    t1.daemon=True
    t1.start()

def end_shift_only_a():
    t1=threading.Thread(target=end_shift_thread)
    t1.daemon=True
    t1.start()

def end_shift_report():
    t1=threading.Thread(target=end_shift_report_thread)



#schedule.every().hour.at(":25").do(end_shift_only )
#schedule.every().day.at("16:00").do(end_shift_only)
schedule.every().day.at("18:30").do(end_shift_only)
schedule.every().day.at("05:00").do(end_shift_only_b)
curr_prod_order = 0
last_update=db.last_update()
prewindow = [[sg.Text('Our Database was last updated from SAP on '+str(last_update)+', Do you wish to update now?')],
          [sg.Button('Yes'), sg.Button('No')]]


window = sg.Window('Sync', prewindow)
while True:
    event,values = window.read()
    if event==None:

        break
    if event=='Yes':
        sap_com.recv_sap()
        db.log_update()
        break
    if event=='No':
        break
window.close()

ack_value=[False,False,False,False,False,False,False,False,False,False]
interlock_times = [0,0,0,0,0,0,0,0,0,0]
details_db = []
def round_up(number):
    number = number*100
    number = int(number)
    number = float(number)/100
    return(number)

status=[0,0,0,0,0,0,0,0,0,0]
prev_reject=[0,0,0,0,0,0,0,0,0,0]
prev_rework=[0,0,0,0,0,0,0,0,0,0]
main_loop_run=False
mach_names = ["M02CN005", "M02CN006", "M02CN007", "M02CN008", "M02CN009", "M02CN010", "M02CN011", "M02CN012","M02CN013", "M02HB004"]
downtime_starts=[0]*10
while True:
    schedule.run_pending()
    shift=get_curr_shift()
    print(shift)
    if shift==False:
        main_loop_run=False
    else:
        #print(shift_now)
        main_loop_run=True
    if main_loop_run==True:
        #curr_prod_order= db.get_all_curr_orders()
        #gui_backend.update_current_orders(window,curr_prod_order)
        for i in range(10):
            try:
                machine = mach_names[i]
                all_val = plc_com.get_plc_data(i)
                print(all_val)
                value_1=all_val[0]
                shifts=all_val[1]
                inter_value = all_val[2]
                pc_vale=all_val[3]
                ack_value = all_val[4]
                #print(ack_value)
                total_time = int(value_1[3])*60
                idle_time  = int(value_1[5])*60
                avg_cycle_time = value_1[6]
                try:
                    availibility = total_time/(total_time+idle_time)
                    availibility = round_up(availibility)
                except:
                    availibility =  0
                try:
                    target_production = total_time/avg_cycle_time
                    #print(target_production)
                    target_production = round_up(target_production)
                except:
                    target_production = 0
                try:
                    performance = value_1[0]/target_production
                    performance  = round_up(performance)
                except:
                    performance = 0
                try:
                    quality = value_1[4]/value_1[0]
                    quality = round_up(quality)
                except:
                    quality = 0
                oee = availibility*performance*quality*100
                oee = round_up(oee)

                stat = apis.check_status(i)
                print(' MACHINE: '+str(i)+' : '+str(stat)+','+str(status[i]))
                #print(ack_value)
                if int(stat) == 0:
                    if status[i]==0:
                        print("no order")
                        #plc_com.change_pc(i, 1)
                    elif status[i]==1:
                        print('machine stopped')
                        status[i]=0
                        print('machine status changed to:'+str(status[i]))
                        plc_com.change_pc(i,1)
                        print("producation change trigger set to High")
                        plc_com.change_ack_value(i, 0)
                        print("Acknowledge trigger set to Low")
                        prev_reject[i] = value_1[2]
                        prev_rework[i] = value_1[1]
                        print('Internal rejection and rework counters set to low')
                        if shift=='A':
                            db.put_in_sap(str(i), str(value_1[0]), shift,value_1[6], 1)
                        else:
                            db.put_in_sap(str(i), str(value_1[0]), shift, value_1[6],0)
                elif (int(stat) == 1):
                    if status[i] == 0:
                        status[i] = 1
                        print('machine started')
                        order=apis.get_curr_order(machine)
                        print(order)
                        db.update_curr_order(str(i),order)
                        prev_reject[i] = value_1[2]
                        prev_rework[i] = value_1[1]
                        plc_com.change_pc(i, 0)
                        plc_com.change_ack_value(i, 0)
                        print('plc_signal pulled down')
                    elif status[i]==2:
                        print('get downtime reason')
                        downtime_starts = db.get_mach_start(str(i))
                        print(downtime_starts)
                        duration=int(value_1[3])-int(downtime_starts[0])
                        reason = apis.update_downtime(mach_names[i],str(datetime.datetime.now()),str(value_1[3]),str(duration))
                        status[i]=1
                        plc_com.change_ack_value(i, 0)
                        print(duration)
                        print(value_1[3])
                        db.add_downtime_update(str(i),str(value_1[3]),reason,str(duration))
                        db.update_downtime_sap(str(i),reason,duration)
                    elif status[i]==4:
                        print('get rejection reason')
                        status[i]=1
                        plc_com.change_ack_value(i,0)
                        prev_reject[i] = value_1[2]
                        print(apis.get_rejection_reason(mach_names[i]))
                    elif status[i]==5:
                        print('get rework reason')
                        status[i]=1
                        plc_com.change_ack_value(i,0)
                        prev_rework[i] = value_1[1]
                        print('updated value of prev rework',str(prev_reject[i]))
                        print(apis.get_rework_reason(mach_names[i]))
                    elif status[i]==1:
                        print('ack_value',(ack_value))
                        if ack_value[0] == True:
                            print('machine_rew value:'+str(value_1[1]))
                            print('variable rew value:'+str(prev_rework[i]))
                            print('machine_rej value:'+str(value_1[2]))
                            print('variable rej value:'+str(prev_reject[i]))
                            if value_1[1] > prev_rework[i]:
                                print('rework detected')
                                print(apis.send_stat([mach_names[i], "5"]))
                                print('rework status sent')
                                db.inc_rework(str(i))
                                print('updated in db')
                                prev_rework[i] = value_1[1]
                                print(prev_rework[i])
                                status[i]=5
                            elif value_1[2] > prev_reject[i]:
                                print('rejection detected')
                                print(apis.send_stat([mach_names[i], "4"]))
                                print('rejection status sent')
                                db.inc_rejection(str(i))
                                print('updated in db')
                                prev_reject[i] = value_1[2]
                                print(prev_reject[i])
                                status[i]=4
                            else:
                                print('strart interlock')
                                apis.send_stat([mach_names[i], "2"])
                                #downtime_starts[i]=str(value_1[3])
                                print("interlocking status sent")
                                db.add_curr_downtime(str(i), shift, value_1[3])
                                status[i]=2
                                print(apis.post_down(shift, mach_names[i], str(datetime.datetime.now()), str(value_1[3])))

                        try:
                            apis.post_machp(shift, machine, value_1[0], value_1[4], value_1[2], value_1[1], value_1[6], availibility, performance, quality, value_1[5], oee)
                        except:
                            pass

                elif int(stat) == 2:
                    if status[i]==2:
                        pass
                    else:
                        order = apis.get_curr_order(machine)
                        db.update_curr_orderntime(str(i),order)
                        prev_reject[i] = value_1[2]
                        prev_rework[i] = value_1[1]
                        status[i]=2


                elif int(stat) == 3:
                    if status[i] == 3:
                        pass
                    else:
                        order = apis.get_curr_order(machine)
                        db.update_curr_orderntime(str(i), order)
                        prev_reject[i] = value_1[2]
                        prev_rework[i] = value_1[1]
                        status[i] = 3

                elif int(stat) == 4:
                    if status[i] == 4:
                        pass
                    else:
                        order = apis.get_curr_order(machine)
                        db.update_curr_orderntime(str(i), order)
                        status[i] = 4

                elif int(stat) == 5:
                    if status[i]== 5:
                        pass
                    else:
                        prev_reject[i] = value_1[2]
                        prev_rework[i] = value_1[1]
                        order = apis.get_curr_order(machine)
                        db.update_curr_orderntime(str(i), order)
                        status[i] = 5
                apis.send_plc_status(1)


            except Exception as e:
                print(e)
                try:
                    apis.send_plc_status(0)
                except Exception as e:
                    print(e)
    else:
        time.sleep(5)



