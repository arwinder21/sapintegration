import requests
import json
import stomp
machs = 10
mach_status=[]
mach_names = ['M02CN005', 'M02CN006', 'M02CN007', 'M02CN008', 'M02CN009', 'M02CN010', 'M02CN011', 'M02CN012','M02CN013', 'M02HB004']
#base_ip_l="http://18.220.175.245:8080/SapPms-0.0.1-SNAPSHOT"
for i in range(machs):
    mach_status.append(0)

class MyListener(stomp.ConnectionListener):
    def on_error(self, headers, message):
        print('received an error "%s"' % message)

    def on_message(self, headers, message):
        global mach_status, mach_names
        if headers['destination'] == '/topic/chat.sendMachineStatus':
            name = (message)
            temp = json.loads(name)
            id = temp['machine_code']
            status = temp['status']
            id = mach_names.index(id)
            mach_status[int(id)] = int(status)
            print(mach_status)
base_ip_l="http://localhost:8080/SapPms-0.0.1-SNAPSHOT"
#base_ip="http://192.168.43.143:8065"
url_check_stat = str(base_ip_l)+"/api/getMachineStatus?machineCode="
def check_status_initial(mac):
    global url_check_stat
    url=url_check_stat+str(mac)
    stat = requests.get(url,timeout = 20)
    #print(stat.text)
    return(json.loads(stat.text)["status"][0]['status'])


for i in range(machs):
    mach_status[i]=int(check_status_initial(mach_names[i]))

conn = stomp.Connection([("localhost", 61613)])
listener = MyListener()
conn.set_listener('', listener)
conn.start()
conn.connect('guest', 'guest', wait=True)
conn.subscribe(destination='/topic/chat.sendMachineStatus', id=1, ack='auto')



print(base_ip_l)
url_insert_recv = str(base_ip_l)+"/api/insertrecveproductionorder"
body_insert_recv = {"order_no":"123","work_center":"aaa","material":"jjj","material_description":"na","pending_quantity":"111","draw_no":"DR02","avg_cys_time":"000"}

url_insert_prodc = str(base_ip_l)+"/api/insertproductioncount"
body_insert_prodc = {"count":"12","machinecode":"cnc001"}

url_insert_rejc = str(base_ip_l)+"/api/insertrejectioncount"
body_insert_rejc = {"rcount":"123","machinecode":"cnc001"}

url_insert_rejq = str(base_ip_l)+"/api/insertrejectionquantity"
body_insert_rejq = {"shift":"a","machinecode":"cnc001","cell_description":"abc","drawing_no":"123","rejection_reason":"breakdown","rejection_quantity":"12"}

url_insert_machp = str(base_ip_l)+"/api/insertmachineproduction"
#body_insert_machp = {"shift":"A","""machinecode":"cnc001","total_prod":"12121","ok_production_quantity":"20","rejection_quantity":"25","rework_quantity":"111","idletime":"8","type":"na","oee":"8"}
body_insert_machp_one = {"machinecode":"M02CN008","total_prod":"200","ok_prod":"150","rej_counter":"50","rework_counter":"50","cycle_time":"50","avail":"50","perform":"75","quality":"100","idletime":"20","oee":"45","shift":"A"}
body_insert_machp = {"machinecode":"M02CN008","total_prod":"200","ok_prod":"150","rej_counter":"50","rework_counter":"50","cycle_time":"50","avail":"50","perform":"75","quality":"100","idletime":"20","oee":"45","shift":"A"}

url_insert_idle = str(base_ip_l)+"/api/insertIdleTime"
body_insert_idle = {"production_order_target_quantity":"10","shift_target_quantity":"15","shift_produced_quantity":"200","shift_rejected_quantity":"20","shift_rework_quantity":"25","machinecode":"cnc001"}

url_insert_down = str(base_ip_l)+"/api/insertdowntime"
body_insert_down = {"shift":"A","machinecode":"cnc001","start_time":"breakdown","start_runtime":"121"}

url_check_stat = str(base_ip_l)+"/api/getMachineStatus?machineCode="

url_check_order = str(base_ip_l)+"/api/getCurrentOrder?machineCode="

url_send_stat = str(base_ip_l)+"/api/insertMachineStatus"
body_send_stat_one = {"machine_status":"mama","machine_code":"amama"}
body_send_stat = {"status":"mama","machine_code":"amama"}
#print(requests.get(url_check_stat).text)

url_get_rejectr= str(base_ip_l)+"/api/getTableRejection?machineCode="

url_get_reworkr= str(base_ip_l)+"/api/getTableRework?machineCode="

url_update_downtime = str(base_ip_l)+"/api/updatedowntime"
body_update_downtime = {"machinecode":"M02CN008","stop_time":"20:20:20","stop_runtime":"20:20:20","duration":"60"}
headers={"Content-Type":"application/json"}

url_plc_stat=str(base_ip_l)+"/api/updatePlcStatus"
body_plc_stat={"status":"0"}

def send_plc_status(stat):
    global url_plc_stat,body_plc_stat
    body_plc_stat["status"] = str(stat)
    api_req = requests.post(url_plc_stat, headers=headers, json=body_plc_stat ,timeout = 20)
    return(api_req.text)

def send_stat_one(data):
    global url_send_stat,body_send_stat
    body_send_stat["machine_status"] = data[1]
    body_send_stat["machine_code"] = data[0]
    api_req = requests.post(url_send_stat, headers=headers, json=body_send_stat ,timeout = 20)
    return(api_req.text)

def send_stat(data):
    global conn,body_send_stat
    body_send_stat["status"] = data[1]
    body_send_stat["machine_code"] = data[0]
    print(body_send_stat)
    conn.send(body=json.dumps(body_send_stat),destination='/exchange/amq.topic/chat.sendMachineStatus')

#for i in mach_names:
#    send_stat([i,'0'])
#
def check_status(mac):
    global mach_status
    return(mach_status[mac])


def get_rejection_reason(mac):
    global url_check_stat
    url=url_get_rejectr+str(mac)
    stat = requests.get(url,timeout = 20)
    return(json.loads(stat.text))

def get_rework_reason(mac):
    global url_check_stat
    url=url_get_reworkr+str(mac)
    stat = requests.get(url,timeout = 20)
    return(json.loads(stat.text))

#print(check_status(mach_names[0]))
def get_curr_order(mac):
    global url_check_stat
    url=url_check_order+str(mac)
    stat = requests.get(url,timeout = 20)
    if json.loads(stat.text)["currentOrder"] == []:
        return(None)
    else:
        return((json.loads(stat.text)["currentOrder"][0]['order_no']))

def post_recv(order_no,work_center,material,material_description,pending_quantity,draw_no,act):
    global url_insert_recv,body_insert_recv
    body_insert_recv["order_no"]=order_no
    body_insert_recv["work_center"]=work_center
    body_insert_recv["material"]=material
    body_insert_recv["material_description"]=material_description
    body_insert_recv["pending_quantity"]=pending_quantity
    body_insert_recv["draw_no"]=draw_no
    body_insert_recv["avg_cys_time"]=act
    api_req = requests.post(url_insert_recv, headers=headers, json=body_insert_recv,timeout = 20)
    print(api_req.text)

def post_prodc(count,machinecode):
    global url_insert_prodc,body_insert_prodc
    body_insert_prodc["count"] = count
    body_insert_prodc["machinecode"] = machinecode
    api_req = requests.post(url_insert_prodc, headers=headers, json=body_insert_prodc,timeout = 20)
    print(api_req.text)

def post_rejc(rcount,machinecode):
    global url_insert_rejc,body_insert_rejc
    body_insert_rejc["rcount"] = rcount
    body_insert_rejc["machinecode"] = machinecode
    api_req = requests.post(url_insert_rejc, headers=headers, json=body_insert_rejc,timeout = 20)
    print(api_req.text)

def post_rejq(shift,machinecode,cell_description,drawing_no,rejection_reason,rejection_quantity):
    global url_insert_rejq,body_insert_rejq
    body_insert_rejq["shift"] = shift
    body_insert_rejq["machinecode"] = machinecode
    body_insert_rejq["cell_description"] = cell_description
    body_insert_rejq["drawing_no"] = drawing_no
    body_insert_rejq["rejection_reason"] = rejection_reason
    body_insert_rejq["rejection_quantity"] = rejection_quantity
    api_req = requests.post(url_insert_rejq, headers=headers, json=body_insert_rejq,timeout = 20)
    print(api_req.text)

def post_machp_one(shift,machinecode,shift_target_production,ok_production_quantity,rejection_quantity,rework_quantity,cycle,avail,perform,quality,idletime,oee):
    global url_insert_machp,body_insert_machp_one
    body_insert_machp["shift"] = shift
    body_insert_machp["machinecode"] = machinecode
    body_insert_machp["total_prod"] = shift_target_production
    body_insert_machp["ok_prod"] = ok_production_quantity
    body_insert_machp["rej_counter"] = rejection_quantity
    body_insert_machp["rework_counter"] = rework_quantity
    body_insert_machp["cycle_time"] = cycle
    body_insert_machp["avail"] = avail
    body_insert_machp["perform"] = perform
    body_insert_machp["quality"] = quality
    body_insert_machp["idletime"] = idletime
    body_insert_machp["oee"] = oee
    #body_inseet_machp["shift"] = shift
    #print(body_insert_machp)
    api_req = requests.post(url_insert_machp, headers=headers, json=body_insert_machp,timeout = 20)
    print(api_req.text)

def post_machp(shift,machinecode,shift_target_production,ok_production_quantity,rejection_quantity,rework_quantity,cycle,avail,perform,quality,idletime,oee):
    global body_insert_machp , conn
    body_insert_machp["shift"] = str(shift)
    body_insert_machp["machinecode"] = str(machinecode)
    body_insert_machp["total_prod"] = str(shift_target_production)
    body_insert_machp["ok_prod"] = str(ok_production_quantity)
    body_insert_machp["rej_counter"] = str(rejection_quantity)
    body_insert_machp["rework_counter"] = str(rework_quantity)
    body_insert_machp["cycle_time"] = str(cycle)
    body_insert_machp["avail"] = str(avail)
    body_insert_machp["perform"] = str(perform)
    body_insert_machp["quality"] = str(quality)
    body_insert_machp["idletime"] = str(idletime)
    body_insert_machp["oee"] = str(oee)
    #body_inseet_machp["shift"] = shift
    #print(body_insert_machp)
    conn.send(body=json.dumps(body_insert_machp),destination='/exchange/amq.topic/chat.sendProduction')


def post_idle(production_order_target_quantity,shift_target_quantity,shift_produced_quantity,shift_rejected_quantity,shift_rework_quantity,machinecode):
    global url_insert_idle,body_insert_idle
    body_insert_idle['production_order_target_quantity'] = production_order_target_quantity
    body_insert_idle['shift_target_quantity'] = shift_target_quantity
    body_insert_idle['shift_produced_quantity'] = shift_produced_quantity
    body_insert_idle['shift_rejected_quantity'] = shift_rejected_quantity
    body_insert_idle['shift_rework_quantity'] = shift_rework_quantity
    body_insert_idle['machinecode'] = machinecode
    api_req = requests.post(url_insert_idle, headers=headers, json=body_insert_idle, timeout = 20)
    print(api_req.text)

def post_down(shift,machinecode,start_time,start_runtime):
    global url_insert_down,body_insert_down
    body_insert_down["shift"] = shift
    body_insert_down["machinecode"] = machinecode
    body_insert_down["start_time"] = start_time
    body_insert_down["start_runtime"] = start_runtime
    api_req = requests.post(url_insert_down, headers=headers, json=body_insert_down,timeout = 20)
    print(api_req.text)
#
def update_downtime(machine_code,stop_time,stop_runtime,duration):
    global url_update_downtime,body_update_downtime,headers
    body_update_downtime["machinecode"]=machine_code
    body_update_downtime["stop_time"]=stop_time
    body_update_downtime["stop_runtime"] = stop_runtime
    body_update_downtime["duration"] = duration
    api_req = requests.post(url_update_downtime, headers=headers, json=body_update_downtime, timeout=10)
    reason = json.loads(api_req.text)["reason"][0]["reason"]
    return(reason)

def send_plc_details_cloud(value_1,shifts,inter_value,pc_vale,ack_value):
    #print('sending value to cloud')
    pass

#post_recv("001000317219","M02CN008","C-M-219-0286-08","PINION KICK STARTER","50000.000","0286","49.000")
#reason = update_downtime('M02CN005','23:12','66','22')
#db.update_downtime_sap('saas',21)


#post_recv("001000317220","M02CN009","C-M-219-0286-09","PINION KICK STARTER","45000.000","0287","50.000")
#post_recv("001000317221","M02CN010","C-M-219-0286-10","PINION KICK STARTER","45000.000","0288","51.000")