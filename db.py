import sqlite3
import datetime, apis


def get_ip():
    return ("192.168.21.156")


mach_names = ['M02CN005', 'M02CN006', 'M02CN007', 'M02CN008', 'M02CN009', 'M02CN010', 'M02CN011', 'M02CN012',
              'M02CN013', 'M02HB004']


def create():
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    querry = '''CREATE TABLE IF NOT EXISTS  sap_inbound

             (
             order_num       char     NULL,
             work_centre     char     NULL,
             material        char     NULL,
             material_desc   char     NULL,
             pending_quant   char     NULL,
             drawing         char     NULL,
             avg_cycle_time  char     NULL
             )'''
    cur.execute(querry)

    querry = '''CREATE TABLE IF NOT EXISTS  current_order

             (

             work_centre    char     NULL,
             start_time     char     NULL,
             order_no       char     NULL,
             curr_stat      char     NULL,
             reject_co      char     NULL,
             rework_co      char     NULL,
             bp_m_s         char     NULL,
             no_op          char     NULL,
             mech_bd        char     NULL,
             qa_check     char     NULL,
             power_c     char     NULL,
             no_air     char     NULL,
             mc_star_t     char     NULL,
             no_mat     char     NULL,
             tool_change     char     NULL,
             elec_bd     char     NULL,
             mc_set_add     char     NULL,
             no_tool     char     NULL,
             coil_bar    char     NULL,
             tea_break     char     NULL
             )'''
    cur.execute(querry)

    orders = []
    for j in cur.execute("SELECT * FROM current_order"):
        orders.append(j)
    if orders == []:
        print('empty')
        for i in range(10):
            querry = '''INSERT INTO current_order values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
            cur.execute(querry,
                        [str(i)] + ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                                    '0', '0'])
            connect.commit()

    querry = '''CREATE TABLE IF NOT EXISTS  misc
             (
             last_update     char     NULL
             )'''
    cur.execute(querry)

    querry = '''CREATE TABLE IF NOT EXISTS  curr_downtime
             (
             machine_no     char     NULL,
             shift     char     NULL,
             order_no          char     NULL,
             downt_time     char     NULL,
             downm_time     char     NULL
             )'''
    cur.execute(querry)

    orders = []
    for j in cur.execute("SELECT * FROM curr_downtime"):
        orders.append(j)
    if orders == []:
        print('empty')
        for i in range(10):
            querry = '''INSERT INTO curr_downtime values (?,?,?,?,?)'''
            cur.execute(querry, [str(i)] + ['0', '0', '0', '0'])
            connect.commit()

    querry = '''CREATE TABLE IF NOT EXISTS  rejection
              (
              date     char     NULL,
              shift     char     NULL,
              mach_no          char     NULL,
              draw_no     char     NULL,
              reject_reason     char     NULL
              )'''
    cur.execute(querry)

    querry = '''CREATE TABLE IF NOT EXISTS  rework
              (
              date     char     NULL,
              shift     char     NULL,
              mach_no          char     NULL,
              draw_no     char     NULL,
              reject_reason     char     NULL
              )'''
    cur.execute(querry)

    querry = '''CREATE TABLE IF NOT EXISTS  downtime
             (
             date     char     NULL,
             shift     char     NULL,
             machine_no     char     NULL,
             order_no          char     NULL,
             downstrt_time     char     NULL,
             downstp_time      char     NULL,
             downmstrt         char     NULL,
             downmstp          char     NULL,
             reason     char     NULL,
             duration     char     NULL
             )'''
    cur.execute(querry)

    querry = '''CREATE TABLE IF NOT EXISTS  send_sap
             (
             prod_order     char     NULL,
             oper_act     char     NULL,
             yield     char     NULL,
             act1     char     NULL,
             act2     char     NULL,
             personal_no       char     NULL,
             posting_date     char     NULL,
             break_time     char     NULL,
             shift     char     NULL,
             rework     char     NULL,
             reject     char     NULL,
             work_hrs     char     NULL,
             bop_m_s     char     NULL,
             no_op     char     NULL,
             mech_bd     char     NULL,
             qa_check     char     NULL,
             power_c     char     NULL,
             no_air     char     NULL,
             mc_star_t     char     NULL,
             no_mat     char     NULL,
             tool_change     char     NULL,
             elec_bd     char     NULL,
             mc_set_add     char     NULL,
             no_tool     char     NULL,
             coil_bar    char     NULL,
             tea_break     char     NULL
             )'''
    cur.execute(querry)

    querry = '''CREATE TABLE IF NOT EXISTS  send_sap_log
             (
             date         char      NULL,
             time         char      NULL,
             machine      char      NULL,
             prod_order     char     NULL,
             oper_act     char     NULL,
             yield     char     NULL,
             act1     char     NULL,
             act2     char     NULL,
             personal_no       char     NULL,
             posting_date     char     NULL,
             break_time     char     NULL,
             shift     char     NULL,
             rework     char     NULL,
             reject     char     NULL,
             work_hrs     char     NULL,
             bop_m_s     char     NULL,
             no_op     char     NULL,
             mech_bd     char     NULL,
             qa_check     char     NULL,
             power_c     char     NULL,
             no_air     char     NULL,
             mc_star_t     char     NULL,
             no_mat     char     NULL,
             tool_change     char     NULL,
             elec_bd     char     NULL,
             mc_set_add     char     NULL,
             no_tool     char     NULL,
             coil_bar    char     NULL,
             tea_break     char     NULL
             )'''
    cur.execute(querry)
    connect.commit()
    connect.close()


def drop_sap_send():
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    querry = cur.execute('''DROP TABLE IF EXISTS  send_sap''')
    connect.commit()
    connect.close()
    create()


def drop_inbound():
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    querry = cur.execute('''DROP TABLE IF EXISTS  sap_inbound''')
    connect.commit()
    connect.close()
    create()


def refresh_sap(data):
    for i in range(len(data)):
        if data[i] == '':
            data[i] = 'NA'
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    querry = '''INSERT INTO sap_inbound values (?,?,?,?,?,?,?)'''
    apis.post_recv(data[0], data[1], data[2], data[3], data[4], data[5], data[6])
    # print(data)
    # apis.post_recv(order_no, work_center, material, material_description, pending_quantity, machine_code)
    cur.execute(querry, data)
    connect.commit()
    connect.close()


def get_all_mach_no():
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    wc = []
    for j in cur.execute("SELECT work_centre FROM sap_inbound "):
        if j[0] not in wc:
            wc.append(j[0])
    connect.commit()
    connect.close()
    return (wc)


def get_all_order_no():
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    orders = []
    for j in cur.execute("SELECT order_num FROM sap_inbound "):
        orders.append(j[0])
    connect.commit()
    connect.close()
    return (orders)


def get_all_curr_orders():
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    orders = []
    for j in cur.execute("SELECT * FROM current_order "):
        orders.append(j)
    connect.commit()
    connect.close()
    return (orders)


def update_curr_order(mach, ord):
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    querry = '''UPDATE current_order SET order_no=?,start_time=? WHERE work_centre=?'''
    time1 = (datetime.datetime.now())
    cur.execute(querry, (ord, time1, mach))
    connect.commit()
    connect.close()


def update_curr_orderntime(mach, order):
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    querry = '''UPDATE current_order SET order_no=? WHERE work_centre=?'''
    # time1=(datetime.datetime.now())
    cur.execute(querry, (order, mach))
    connect.commit()
    connect.close()


def get_curr_mach_order_no(num):
    all_det = get_all_curr_orders()
    order_num = all_det[num][1]
    return (order_num)


def get_order_details(number):
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    orders = []
    for j in cur.execute("SELECT * FROM sap_inbound WHERE order_num=?", number):
        orders.append(j)
    connect.commit()
    connect.close()
    try:
        return (orders[0])
    except:
        return (False)


def get_pending_order_no(num):
    order_n = get_curr_mach_order_no(num)
    details = get_order_details([order_n])
    if details == False:
        return (11010100)
    else:
        return (int(float(details[4])))


def get_mach_details(mach):
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    machines = []
    for j in cur.execute("SELECT * FROM sap_inbound WHERE work_centre=?", mach):
        machines.append(j)
    connect.commit()
    connect.close()
    return (machines)


def last_update():
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    orders = []
    for j in cur.execute("SELECT last_update FROM misc"):
        orders = j
    if orders == []:
        connect.commit()
        connect.close()
        return (False)
    connect.commit()
    connect.close()
    return (orders[0])


def log_update():
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    update_status = last_update()
    if update_status == False:
        querry = '''INSERT INTO misc (last_update) VALUES (?)'''
    else:
        querry = '''UPDATE misc SET last_update=?'''
    up_time = [datetime.datetime.now()]
    cur.execute(querry, up_time)
    connect.commit()
    connect.close()


def update_curr_run(i):
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    print(i)
    querry = '''UPDATE sap_inbound SET is_running=1 WHERE order_num=(?)'''
    cur.execute(querry, [i, ])
    connect.commit()
    connect.close()


def downtime_log(data):
    connect = sqlite3.connect("sap_db.db")
    data += ['NA']
    cur = connect.cursor()
    querry = '''INSERT INTO downtime values (?,?,?,?,?,?,?)'''
    cur.execute(querry, data)
    connect.commit()
    connect.close()


def get_data_from_current(mach):
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    machines = []
    for j in cur.execute("SELECT * FROM current_order WHERE work_centre=?", mach):
        machines.append(j)
    connect.commit()
    connect.close()
    print(machines)
    a = [machines[0][2]] + list(machines[0][4:])
    b = machines[0][1]
    return (a, b)


def reset_sap_mach(i):
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    machines = []
    querry = "UPDATE current_order SET order_no=?,curr_stat=?,reject_co=?,rework_co=?,bp_m_s=?,no_op=?,mech_bd=?,qa_check=?,power_c=?,no_air=?,mc_star_t=?,no_mat=?,tool_change=?,elec_bd=?,mc_set_add=?,no_tool=?,coil_bar=?,tea_break=? WHERE work_centre=?"

    values = ('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', str(i))
    cur.execute(querry, values)
    connect.commit()
    connect.close()


def str_to_tup(t):
    halves = t.split()
    dates = halves[0].split('-')
    times = halves[1].split(':')
    alls = dates + times
    ret_val = datetime.datetime(int(alls[0]), int(alls[1]), int(alls[2]), int(alls[3]), int(alls[4]),
                                int(float(alls[5])))
    return (ret_val)


def normalise_two(argu):
    a = argu * 100
    print(a)
    a = int(a)
    print(a)
    a = float(a) / 100
    return (a)


def put_in_sap(mach, total_prod, curr_shift, avg_cycle_time, n):
    data_all = get_data_from_current(mach)
    data = data_all[0]
    start_time = data_all[1]
    prod_order = data[0]
    if int(prod_order) == 0:
        pass
    else:
        start_tuple = str_to_tup(start_time)
        datetime_all = datetime.datetime.now()
        work_hrs = str(normalise_two(((datetime_all - start_tuple).total_seconds()) / (3600)))
        date = str(datetime_all.date())
        time = str(datetime_all.time())
        machine = mach_names[int(mach)]
        oper_act = '10'
        personal_no = '0'
        break_time = '30'
        # work_hrs='12'
        yield_quant = total_prod
        act1 = act2 = str(normalise_two(float(avg_cycle_time) * float(total_prod) / 60.0))
        if n == 0:
            temp = str((datetime.datetime.now() - datetime.timedelta(1)).date())
            posting_date = temp[0:4] + temp[5:7] + temp[8:10]
        else:
            temp = str(datetime.datetime.now().date())
            posting_date = temp[0:4] + temp[5:7] + temp[8:10]
        break_time = '0'
        sap_prep = [prod_order, oper_act, yield_quant, str(act1), str(act2), personal_no, posting_date, break_time,
                    curr_shift + '-SHIFT'] + data[1:3] + [work_hrs] + data[3:]
        sap_prep_log = [date, time, machine] + sap_prep
        connect = sqlite3.connect("sap_db.db")
        cur = connect.cursor()
        querry = '''INSERT INTO send_sap values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
        querr1 = '''INSERT INTO send_sap_log values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
        print(sap_prep)
        print(sap_prep_log)
        cur.execute(querry, sap_prep)
        cur.execute(querr1, sap_prep_log)
        connect.commit()
        connect.close()
        reset_sap_mach(mach)


# put_in_sap('0','121','A',33,0)

def fetch_sap_Send():
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    machines = []
    for j in cur.execute("SELECT * FROM send_sap "):
        machines.append(j)
    connect.commit()
    connect.close()
    val = []
    for i in machines:
        temp = ''
        for j in i:
            if temp == '':
                temp += (j)
            else:
                temp += (',' + j)
        val.append(temp)
        reset_sap_mach(i)
    return (val)


def add_curr_downtime(mach, shift, downm_time):
    data = get_data_from_current(mach)[0]
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    querry = '''UPDATE curr_downtime SET shift=?,order_no=?,downt_time=?,downm_time=? WHERE machine_no=''' + str(mach)
    cur.execute(querry, [shift, str(data), str(datetime.datetime.now().time()), downm_time])
    connect.commit()
    connect.close()


def get_curr_down_data(mach):
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    machines = []
    for j in cur.execute("SELECT * FROM curr_downtime WHERE machine_no=" + str(mach)):
        machines.append(j)
    connect.commit()
    connect.close()
    return (machines[0])


def add_downtime_update(mach, downm_stop, reason, duration):
    data = list(get_curr_down_data(mach))
    date = str(datetime.datetime.now().date())
    print((str(date), data[1], mach_names[int(mach)], data[2], data[3], str(datetime.datetime.now().time()), data[4],
           downm_stop, reason, duration))
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    querry = '''INSERT INTO downtime values (?,?,?,?,?,?,?,?,?,?)'''
    cur.execute(querry, (
    str(date), data[1], mach_names[int(mach)], data[2], data[3], str(datetime.datetime.now().time()), data[4],
    downm_stop, reason, duration))
    connect.commit()
    connect.close()


def inc_rejection(mach):
    data = get_data_from_current(mach)[0]
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    new_value = int(data[1]) + 1
    querry = '''UPDATE current_order SET reject_co=? WHERE work_centre=''' + str(mach)
    cur.execute(querry, (str(new_value),))
    connect.commit()
    connect.close()


# inc_rejection('0')
def inc_rework(mach):
    data = get_data_from_current(mach)[0]
    print(data)
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    new_value = int(data[2]) + 1
    querry = '''UPDATE current_order SET rework_co=? WHERE work_centre=''' + str(mach)
    cur.execute(querry, (str(new_value),))
    connect.commit()
    connect.close()


def get_mach_start(mach):
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    machines = []
    for j in cur.execute("SELECT downm_time FROM curr_downtime WHERE machine_no=" + str(mach)):
        machines.append(j[0])
    connect.commit()
    connect.close()
    return (machines)


# get_mach_start(str(0))
def update_downtime_sap(mach, reason, duration):
    connect = sqlite3.connect("sap_db.db")
    cur = connect.cursor()
    reason_map = {"BOP Material Short": "bp_m_s", "No Operator": "no_op", "Mechnical BD": "mech_bd",
                  "QA Checking": "qa_check", "Power Cut": "power_c", "No Air": "no_air", "M/C Start Time": "mc_star_t",
                  "No Material": "no_mat", "Tool Change": "tool_change", "Electrical BD": "elec_bd",
                  "M/C Setting Adjust": "mc_set_add", "No Tool": "no_tool", "Coil Bar Load/Unload": "coil_bar",
                  "JH Act./Tea Break": "tea_break"}
    machines = []
    for j in cur.execute("SELECT " + reason_map[reason] + " FROM current_order WHERE work_centre=" + str(mach)):
        machines.append(j[0])
    old = int(machines[0])
    new = old + int(duration)
    querry = '''UPDATE current_order SET ''' + reason_map[reason] + '''''''=? WHERE work_centre=''' + str(mach)
    cur.execute(querry, (str(new),))
    connect.commit()
    connect.close()
    print(reason_map[reason])


create()

# add_downtime_update('0','11','aa','21')
