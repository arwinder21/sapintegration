from pyModbusTCP.client import ModbusClient
import win_inet_pton
import db
####### inititate PLC ##########
plc_obj=''
def initiate_plc():
    global ip_str,plc_obj
    print("creating plc client")
    print(ip_str)
    plc_obj = ModbusClient(host=ip_str, port=502, auto_close=True,timeout=5)
    print("plc_object created")
######## get ip from db ########
ip_str = db.get_ip()
########call inititate function####
initiate_plc()
#####Set Register values #######
counter_start = 32
interlock_reg = 234
produc_change_reg = 4
ack_reg = 43
######### getting all register values#####
def get_register_values(machine):
    global counter_start,interlock_reg,produc_change_reg,ack_reg
    if machine == 9:
        counter_start_curr = 136
        interlock_reg_curr = 47
        produc_change_reg_curr = 17
        ack_reg_curr =56
    else:
        counter_start_curr = counter_start+(machine*8)
        interlock_reg_curr = interlock_reg + machine
        produc_change_reg_curr = produc_change_reg + machine
        ack_reg_curr = ack_reg + machine
    return([counter_start_curr,interlock_reg_curr,produc_change_reg_curr,ack_reg_curr])

##########getting all plc values########
def get_plc_data(machine):
    reg_addr=get_register_values(machine)
    plc_data=[]
    plc_obj.open()
    plc_data = (plc_obj.read_holding_registers(reg_addr[0],7))
    if machine==9:
        plc_data[0]=4*plc_data[0]
        plc_data[4]=plc_data[0]-plc_data[2]-plc_data[1]
        plc_data[6]=plc_data[6]/4
    plc_obj.open()
    shift_times = (plc_obj.read_holding_registers(224,4))
    plc_obj.open()
    interlock_value = (plc_obj.read_holding_registers(reg_addr[1],1))
    plc_obj.open()
    produc_change_value = (plc_obj.read_coils(reg_addr[2],1))
    plc_obj.open()
    ack_Value = (plc_obj.read_coils(reg_addr[3],1))
    #print(reg_addr)
    return(plc_data,shift_times,interlock_value,produc_change_value,ack_Value)


############send all shift times#######
def send_shift_times(data):
    global plc_obj
    plc_obj.open()
    plc_obj.write_single_register(224,data[0])
    plc_obj.open()
    plc_obj.write_single_register(225,data[1])
    plc_obj.open()
    plc_obj.write_single_register(226,data[2])
    plc_obj.open()
    plc_obj.write_single_register(227,data[3])

###########send interlockaing signal #######
def send_interlocking_signal(data):
    global plc_obj,interlock_reg
    plc_obj.open()
    plc_obj.write_single_register(interlock_reg,data)

def change_pc(machine,value):
    global plc_obj
    if machine==9:
        reg_addr = [0,0,17]
    else:
        reg_addr = get_register_values(machine)
    #plc_obj.open()
    #print(reg_addr[2])
    #curr_value = plc_obj.read_coils(reg_addr[2],1)
    #print(curr_value)
    plc_obj.open()
    plc_obj.write_single_coil(reg_addr[2],value)

def get_prod_value():
    global plc_obj,produc_change_reg
    reg_addr = get_register_values(0)
    plc_obj.open()
    curr_value = plc_obj.read_coils(reg_addr[2], 9)
    plc_obj.open()
    curr_value+=plc_obj.read_coils(17,1)
    return(curr_value)


def change_ack_value(machine,value):
    global plc_obj
    reg_addr=get_register_values(machine)
    plc_obj.open()
    plc_obj.write_single_coil(reg_addr[3],value)

